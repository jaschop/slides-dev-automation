# slides-dev-automation

Slides for a talk on development automation, first held Nov 2023 at UniBw.

## reveal.js

Built with [reveal.js](https://github.com/hakimel/reveal.js) (v4.5.0) !

Installation method: download release and unzip at `./reveal.js/`
