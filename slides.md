## Development Automation <br/> from Scratch

#### Johann A. Schöpfer
#### johann.schoepfer@tum.de

---

#### Follow Along Online

https://jaschop.codeberg.page/slides-dev-automation

--

## About Me

- Software Developer
- Space Networking Researcher
- I (used to) offer RSE consulting as part of founders@unibw

--

## Contents

 - Tooling Setup from Scratch
 - Pipeline Setup & Debugging
 - An Automated Testing Workflow

---

## Tooling Setup

#### You don't have to do this for yourself!

 - [UniBw GitLab](https://athene2.informatik.unibw-muenchen.de) (INF2)
 - [GitHub](https://github.com) (Microsoft)
 - [Codeberg.org](https://codeberg.org) (Codeberg e.V.)

--

## Codeberg.org <img src="img/Codeberg-logo.svg" height="65em" style="margin-bottom: 0px;"/>

<img src="img/codeberg-screenshot.png" width=75%/>

--

## Codeberg.org <img src="img/Codeberg-logo.svg" height="65em" style="margin-bottom: 0px;"/>

### Forgejo <img src="img/Forgejo-logo.svg" height="50em" style="margin-bottom: 0px;"/>
 - a.k.a. Gitea
 - equivalent to "a GitHub"/"a GitLab"

 ----

### Woodpecker CI    <img src="img/Woodpecker-logo.svg" height="50em" style="margin-bottom: 0px;"/>
 - a.k.a. Drone CI
 - "GitHub Actions"/"GitLab Pipelines"

---

## Prerequisistes

<img src="img/ServerNetworkSetup.svg" width="80%"/>

--

### At UniBw

- virtual severs can be ordered at RZ
- self-managed hardware also possible
- developer access only from Intranet/VPN
- web-access from everywhere (if you ask nicely)
- as many DNS aliases as you need

---

## Installing Forgejo

<img src="img/ForgejoDockerScreenshot.png" width="80%"/>

#### Hold on, what is a container?

--

## Tech: Docker <img src="img/Docker-logo.svg" height="65em" style="margin-bottom: 0px;"/>

#### What is a Container?

#### What is a Container Image?

#### How do I Containerize my Code?

#### What is a Docker Repository?

--

## Container Configuration

docker-compose.yml

    services:
      forgejo-server:
        image: codeberg.org/forgejo/forgejo:1.20
        restart: always
        volumes:
          - ./data/forgejo:/data
          - /etc/timezone:/etc/timezone:ro
          - /etc/localtime:/etc/localtime:ro
        ports:
          - '3000:3000'
          - '222:22'

--

## YAML

#### Yet Another Markup Language

<img src="img/yaml-example.png" width="80%"/>

YAML Objects = nested Maps & Lists

--

## Web Server Config
#### (nginx)

    server {
        listen 80;
        listen [::]:80;
        server_name forge.prototyping.bwi.unibw-muenchen.de;

        location / {
            return 301 https://$host$request_uri;
        }
    }
    server {
        listen 443 ssl;
        listen [::]:443 ssl;
        server_name forge.prototyping.bwi.unibw-muenchen.de;
        ssl_certificate /etc/letsencrypt/live/forge.prototyping.bwi.unibw-muenchen.de/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/forge.prototyping.bwi.unibw-muenchen.de/privkey.pem;

        location / {
            proxy_pass http://127.0.0.1:3000/;
        }
    }

--

## Voila

[Forgejo Server](https://forge.prototyping.bwi.unibw-muenchen.de)

---

## Installing Woodpecker

    docker pull woodpeckerci/woodpecker-server:latest
    docker pull woodpeckerci/woodpecker-agent:latest

--

## Container Configuration

    woodpecker-server:
      image: woodpeckerci/woodpecker-server:latest
      restart: always
      ports:
        - '3001:3001'
        - '3002:3002'
      networks:
        - ci-lab
      volumes:
        - ./data/woodpecker-server-data:/var/lib/woodpecker/
      environment:
        - WOODPECKER_HOST=https://ci.prototyping.bwi.unibw-muenchen.de
        - WOODPECKER_GRPC_ADDR=:3002
        - WOODPECKER_GITEA_URL=https://forge.prototyping.bwi.unibw-muenchen.de
        - WOODPECKER_GITEA_CLIENT=533e2ba4-7254-4a5e-ba28-368f316f79ab
        - WOODPECKER_GITEA_SECRET=[FORGEJO SECRET]
        - WOODPECKER_AGENT_SECRET=[AGENT SECRET]

    woodpecker-agent:
      image: woodpeckerci/woodpecker-agent:latest
      restart: always
      networks:
        - ci-lab
      volumes:
        - ./data/woodpecker-agent-config:/etc/woodpecker
        - /var/run/docker.sock:/var/run/docker.sock
      environment:
        - WOODPECKER_SERVER=woodpecker-server:3002
        - WOODPECKER_AGENT_SECRET=[AGENT SECRET]

--

## Web Server Config
#### (nothing new here)

    server {
        listen 80;
        listen [::]:80;
        server_name ci.prototyping.bwi.unibw-muenchen.de;

        location / {
            return 301 https://$host$request_uri;
        }
    }
    server {
        listen 443 ssl;
        listen [::]:443 ssl;
        server_name ci.prototyping.bwi.unibw-muenchen.de;
        ssl_certificate /etc/letsencrypt/live/ci.prototyping.bwi.unibw-muenchen.de/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/ci.prototyping.bwi.unibw-muenchen.de/privkey.pem;

        location / {
            proxy_pass http://127.0.0.1:3001/;
        }
    }

--

## Let's see it

[WoodpeckerCI Server](https://ci.prototyping.bwi.unibw-muenchen.de)

---

## Setting Up Pipelines

#### .woodpecker.yml

    steps:
      qa:
        image: python:3.9-alpine
        commands:
          - pip install -r requirements.txt
          - nosetests
        when:
          - event: push
            branch: master

--

## Base Images

- Basic Linux Environments
    - alpine
    - debian
    - etc...
- Programming Languages
    - Python
    - Node/JS
    - Rust
    - Matlab

--

## Reasons to run a Pipeline

- Testing
- Building Artefacts
    - such as Docker Images
- Deployment
- Static Code Analysis ("linting")
- License Housekeeping ([REUSE](https://reuse.software))

--

## Documentation Effect

Automating a process not only makes it faster.

It documents it __in code__.

---

## Thank you!

#### Questions?

--

#### Interested in RSE Networking?

Munich RSE Meetup: [Mailing List](https://lists.lrz.de/mailman/listinfo/rse)

